# Formation Docker: session "débutants"

> Nous vous recommandons de bien lire l'ensemble de la consigne avant de démarrer un exercice.

## Exercice 01

### Travail à réaliser

Vous allez devoir lancer trois conteneurs sur votre machine : nginx, mysql et httpd

Pour chacun d'entre eux, vous utiliserez les deux options suivantes à chaque fois :

```sh
--detach
--name
```

Pour ce qui est des des ports d’écoute, vous devrez configurer les conteneurs de la manière suivante :

- nginx avec le mapping 8001:80
- httpd avec le mapping 8002:80
- mysql avec le mapping 3306:3306

Lorsque vous lancerez le conteneur mysql, utilisez l’option `--env` afin de lui faire passer le couple variable d'environnement et valeur `MYSQL_RANDOM_ROOT_PASSWORD=yes` ([cf. ici pour la doc.](https://hub.docker.com/_/mysql))

Terminez en utilisant les commandes `docker container stop` et `docker container rm` pour tout nettoyer :)

### Question pour cet exercice

1. Quel est le mot de passe généré aléatoirement par Docker ?

Tip : Utilisez la commande `docker container logs` sur le conteneur `mysql`afin de récupérer le mot de passe root qui a été généré aléatoirement.

## Exercice 02

### Travail à réaliser

Pour cet exercice, vous utiliserez deux fênetres de terminal en même temps, avec le démarrage des conteneurs :

- ubuntu:20.04 en utilisant le `--publish 8001:80` et l’option `-it`
- ubuntu:22.04 en utilisant le `--publish 8002:80` et l’option `-it`

Utilisez les noms :

`webserver_2004` pour le conteneur exécutant Ubuntu 20.04
`webserver_2204` pour le conteneur exécutant Ubuntu 22.04

Installez le binaire `curl` dans les deux conteneurs Ubuntu au cas où il ne serait pas déjà installé (rappel: `apt-get -y install curl`)

Installez le serveur web nginx sur ubuntu:20.04 et Apache 2 sur ubuntu:22.04

```sh
# ubuntu 20.04 
apt-get update && apt-get install nginx –y && service nginx start

# ubuntu 22.04
apt-get update && apt-get install apache2 –y && service apache2 start
```

### Questions pour cet exercice

Exécutez la commande `curl` de cette manière :

Sur `webserver_2204` :

```sh
curl webserver_2004
```

Sur `webserver_2004` :

```sh
curl webserver_2204
```

1. Est-ce que cela fonctionne ?

Créez maintenant un nouveau réseau appelé `network_test`

Connectez les deux conteneurs toujours en fonctionnement à ce nouveau réseau (rappel de la syntaxe : `docker network connect <mon_reseau> <mon_nom_de_conteneur>`).

2. Est-ce que cela fonctionne si vous exécutez à nouveau les commandes `curl` ?

## Exercice 03 : Créer son premier Dockerfile

### Travail à réaliser

Pour cet exercice, nous dériverons de l'image de base `bitnami/minideb`. Cette image Docker, publiée et maintenue par VMware, s'appuie sur Debian. 

Elle a été développée spécifiquement pour être une image de base minimaliste dans le cadre d'une utilisation pour des conteneurs.

Commencez donc par indiquer la partie `FROM` puis spécifiez, grâce à l'instruction `RUN`, les différentes commandes à exécuter pour installer le binaire `curl` et le service `nginx`.

Exposez ensuite le port 80. Vous mapperez ce port au port 8081 sur l'hôte.

Indiquez le répertoire `/var/www/html` comme `WORKDIR`.

/!\ vérifier le répertoire si pas /usr/share/nginx/html ?

Copiez ensuite à l’intérieur de ce répertoire le fichier `exemple.html` en le renommant `index.html`

Pour démarrer le conteneur, vous utiliserez la commande CMD suivante : 

`CMD ["nginx", "-g", "daemon off;"]`

Buildez (`docker build [...]`) ensuite votre image en l’appelant mon_serveur_web et en utilisant le tag latest.

Lancez l’image et vérifiez que vous obtenez la bonne page en lançant `http://localhost:8081` depuis votre navigateur.

### Question pour cet exercice

1. Quel message obtenez vous dans la page web ?

## Exercice 04 : Utilisation des volumes

Une page HTML se trouve dans le dépôt GitLab. Elle s'appelle `exemple.html`. 

Nous allons utiliser l’image httpd que vous pouvez trouver sur le DockerHub. Elle permet de provisionner un serveur web Apache2.

Jetez un oeil à la section "[How to use this image](https://hub.docker.com/_/httpd)" de la documentation sur le DockerHub pour savoir quel est le répertoire de l’image que nous allons binder avec le répertoire où se trouve actuellement la page `exemple.html` située dans l’exercice 03.

Le conteneur devra avoir le nom `mon_apache` et utiliser l’image `httpd:2.4`.

Pour rappel, grâce à l’option `–v` vous allez pouvoir binder le répertoire dans lequel vous vous situé (attention à mettre le chemin complet entre `''` pour que les espaces soient bien interprétés).

Vérifiez ensuite que le site Internet s’affiche bien. Modifiez la page d’accueil depuis votre répertoire actuel, et vérifiez que le changement a été répercuté en direct en actualisant la fenêtre de votre navigateur.

### Question pour cet exercice

1. Quelle est la commande à taper pour exécuter le conteneur ?